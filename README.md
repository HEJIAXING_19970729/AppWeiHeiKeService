# AppWeiHeiKeService

#### 介绍
校园小程序

#### 软件架构
软件架构说明


#### 安装教程

1.  安装 mysql 5.7
2.  maven 配置阿里云镜像

#### 使用说明

1.  application.yml 中修改自己的mysql 账号密码
2.  generatorConfig.xml 修改表的配置,实体和mapper,mapper.xml会自动生成（执行maven中的Plugins中mybatis-generator）
3.  运行启动类 Application.java 文件
4. mvn clean install -DskipTests 进行打包
5.  打包后jar文件 传到服务器 执行 sudo nohub  java -jar jar包
#### 参与贡献

1.  Fork 本仓库
2.  新建 release_xxx(姓名) 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)