package com.weiheike.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import io.swagger.annotations.Api;


@RestController
@Api(tags = "测试")
public class WeiHeiKeController {
    @GetMapping("/heike")
    public String Weiheike() { return "ok"; }
}
